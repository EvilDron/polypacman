#pragma once

#include <memory>
#include <stack>
#include "State.hpp"

namespace PolyPacman
{
	typedef std::unique_ptr<State> StateRef;
	class StateMachine
	{
	public:
		StateMachine();
		~StateMachine() = default;

		void addState(StateRef newState, bool isReplacing = true);
		void removeState();

		void proccessStateChanges();

		StateRef &getActiveState();

	private:
		std::stack<StateRef> states_;
		StateRef newState_;

		bool isRemoving_;
		bool isAdding_;
		bool isReplacing_;
	};

}
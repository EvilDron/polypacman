
#pragma once

#include <SFML/Graphics.hpp>
#include "Board.hpp"

namespace PolyPacman
{

	enum struct Direction
	{
		NONE,
		UP,
		DOWN,
		LEFT,
		RIGHT
	};

	class Pacman
	{
	public:
		void init(const Direction &direction, BoardRef board);
		void update(const float dt);
		void draw(sf::RenderWindow& window);
		void setNextDirection(const Direction & direction); // ���������� ��������� �����������
		sf::FloatRect getGlobalBounds();
	private:
		void updateShape(); // ��������� �����
		void setDirection(const Direction & direction);
		void tryRotate(); // ���������� �����������, ���� ���� ���� ����
		BoardRef board_;	//������ �� �����
		sf::ConvexShape topShape_; //������� ����� �������
		sf::ConvexShape bottomShape_; //������ ����� �������
		Direction direction_;
		Direction nextDirection_;
		std::size_t eatenCookies_;
		sf::Vector2f position_;
		float orientationDegrees_;
		float animationPhase_; // ���� �������� ���
		
	};

}
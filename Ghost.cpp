#include "Ghost.hpp"
#include "Definitions.hpp"

namespace PolyPacman
{
	Ghost::Ghost(GhostId ghostId, GameDataRef data, BoardRef board) :
		data_(data),
		ghostId_(ghostId),
		board_(board),
		direction_(GhostDirection::NONE)
	{
	}
	void Ghost::init(const sf::Vector2f& position)
	{
		loadTextures();

		direction_ = GhostDirection::NONE;
		shape_.setSize({ GHOST_SIZE, GHOST_SIZE });
		shape_.setPosition(position);
		shape_.setTexture(&sprites_[(int)ghostId_]);
	}
	void Ghost::update(const float dt)
	{
		const float step = GHOST_SPEED * dt;

		sf::Vector2f movement(0.f, 0.f);
		switch (direction_)
		{
		case GhostDirection::UP:
			movement.y -= step;
			break;
		case GhostDirection::DOWN:
			movement.y += step;
			break;
		case GhostDirection::LEFT:
			movement.x -= step;
			break;
		case GhostDirection::RIGHT:
			movement.x += step;
			break;
		case GhostDirection::NONE:
			changeGhostDirection();
			break;
		}
		
		shape_.move(movement);
		const sf::FloatRect ghostBounds = shape_.getGlobalBounds();
		
		if (board_->isWallIntersects(ghostBounds))
		{
			changeGhostDirection();
			shape_.move(-movement);
		}
		
		switch (direction_)
		{
		case GhostDirection::LEFT:
			shape_.setTexture(&sprites_[1]);
			break;
		case GhostDirection::RIGHT:
			shape_.setTexture(&sprites_[0]);
			break;
		case GhostDirection::DOWN:
			shape_.setTexture(&sprites_[3]);
			break;
		case GhostDirection::UP:
			shape_.setTexture(&sprites_[2]);
			break;
		default:
			break;
		}
	}
	void Ghost::draw(sf::RenderWindow& window)
	{
		window.draw(shape_);
	}

	sf::FloatRect Ghost::getGhostBounds() const
	{
		return shape_.getGlobalBounds();
	}
	void Ghost::changeGhostDirection()
	{
			direction_ = static_cast<GhostDirection>(rand()%5); 
	}
	void Ghost::loadTextures()
	{
		char ghostC = 'b';
		switch (ghostId_)
		{
		case GhostId::CLYDE:
			ghostC = 'c';
			break;
		case GhostId::INKY:
			ghostC = 'i';
			break;
		case GhostId::PINKY:
			ghostC = 'p';
			break;
		default:
			break;
		}
		for (size_t i = 1; i <= 4; i++)
		{
			sprites_[i - 1] = data_->assets.getTexture(ghostC+std::to_string(i));
		}
	}
}
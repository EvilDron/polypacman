#pragma once
#include <functional>
#include <SFML/Graphics.hpp>
#include "Game.hpp"

namespace PolyPacman
{
	class Button
	{
	public:
		~Button() = default;
		Button();
		Button(const sf::Vector2f &position,const std::string &text,const sf::Font &font,
					 std::size_t characterSize,const std::function<void()> &action,const sf::Color & textColor = sf::Color::Transparent);
		
		void setPosition(const sf::Vector2f & position); \
		void setPosition(const float x,const float y); 
		void setText(const std::string & text);
		void setFont(const sf::Font & font);
		void setCharacterSize(size_t size);
		void setAction(const std::function<void()> & action); // ���������� �������� ��������
		void setTextFillColor(const sf::Color & color);
		void setTextStyle (const sf::Uint32 & style);
		void draw(sf::RenderWindow& window);
		void execute(); //	����� �������� ������� ������
		sf::Vector2f getPosition() const;
		sf::FloatRect getGlobalBounds() const;
		std::string getText() const;
		

	private:
		sf::Text text_; // ���� ������
		std::function<void()> action_; // ������� ������� ��������� ��� ������� �� ������
	};
	typedef std::shared_ptr<Button> ButtonRef;
}
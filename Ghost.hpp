#pragma once

#include <SFML/Graphics.hpp>
#include "Game.hpp"
#include "Board.hpp"


//#include "Pacman.hpp"
namespace PolyPacman
{

	enum struct GhostDirection
	{
		NONE,
		UP,
		DOWN,
		LEFT,
		RIGHT
	};

	class Ghost
	{
	public:
		Ghost(GhostId ghostId, GameDataRef data, BoardRef board);
		void init(const sf::Vector2f& position);
		void update(const float dt);
		void draw(sf::RenderWindow& window);
		sf::FloatRect getGhostBounds() const;
	private:
		void changeGhostDirection();
		void loadTextures();
		GhostId ghostId_;
		sf::RectangleShape shape_;
		GhostDirection direction_;
		sf::Texture sprites_[4];
		GameDataRef data_;
		BoardRef board_;

	};
	typedef std::shared_ptr<Ghost> GhostRef;
}
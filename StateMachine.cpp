#include "StateMachine.hpp"


namespace PolyPacman
{
	StateMachine::StateMachine() :
		isAdding_(false),
		isRemoving_(false),
		isReplacing_(false)
	{
	}
	void StateMachine::addState(StateRef newState, bool isReplacing)
	{
		isAdding_ = true;
		isReplacing_ = isReplacing;
		newState_ = std::move(newState);
	}

	void StateMachine::removeState()
	{
		isRemoving_ = true;
	}
	void StateMachine::proccessStateChanges()
	{
		if (isRemoving_ && !states_.empty())
		{
			states_.pop();
			if (states_.empty())
			{
				states_.top()->resume();
			}

			isRemoving_ = false;
		}
		if (isAdding_)
		{
			if (!states_.empty())
			{
				if (isReplacing_)
				{
					states_.pop();
				}
				else
				{
					states_.top()->pause();
				}
			}
			states_.push(std::move(newState_));
			states_.top()->init();
			isAdding_ = false;
		}
	}

	StateRef &StateMachine::getActiveState()
	{
		return states_.top();
	}
}
#pragma once
#include <SFML/Graphics.hpp>
#include <vector>

namespace PolyPacman
{
	enum struct CellType // ��� ������
	{
		WALL,
		EMPTY,
		COOKIE,
		SUPERCOOKIE,
	};

	struct Cell // ��������� ����� ������ �� ����
	{
		CellType type;
		sf::RectangleShape shape;
		bool containCookie = false;
	};

	typedef std::shared_ptr<Cell> CellRef; // ����� ��������� ������

	enum class GhostId // ��� ����������
	{
		BLINKY,
		PINKY,
		INKY,
		CLYDE,
	};
	struct BoardGraphics // ��������� ������� ��� ���������
	{
		sf::CircleShape cookieShape;
		sf::CircleShape superCookieShape;
	};

	class Board
	{
	public:
		Board();
		~Board() = default;
		void init(const std::size_t width, const std::size_t height,const std::string field);
		bool isWallIntersects(const sf::FloatRect & rect) const; // �������� �� ������ �����
		bool isCookieIntersects(const sf::FloatRect & rect);// �������� �� ������ �������
		void draw(sf::RenderWindow& window); //��������� ����
		size_t getCookieCount() const; // ���-�� ���������� �������
		sf::Vector2f getPlayerSpawnPos() const; // ��������� ������� �������
		sf::Vector2f getGhostSpawnPos(GhostId ghostId) const; // ��������� ������� ����������
	private:
		void initBoardGraphics(); // ������������� ����������� ����������
		sf::Vector2f getStartPosition(const char sign) const;// ����� ������� �� ������ ����
		sf::Vector2f playerSpawnPoint_; //��������� ������� ������
		std::vector<CellRef> cells_;// ������� ������ ����
		size_t width_; // ������ ����
		size_t height_;// ������ ����
		size_t cookie_count; // ���-�� ������� �� ����
		std::string field_; // ������ �������� ����
		BoardGraphics graphics_; // ����������� ���������
	};

	typedef std::shared_ptr<Board> BoardRef;
}
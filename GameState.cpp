#include "GameState.hpp"
#include "Definitions.hpp"

namespace PolyPacman
{

	GameState::GameState(GameDataRef data) :
		data_(data)
	{
	}
	void GameState::loadGhostsTextures()
	{
		data_->assets.loadTexture("b1", GHOST_RED_1_FILEPATH);
		data_->assets.loadTexture("b2", GHOST_RED_2_FILEPATH);
		data_->assets.loadTexture("b3", GHOST_RED_3_FILEPATH);
		data_->assets.loadTexture("b4", GHOST_RED_4_FILEPATH);

		data_->assets.loadTexture("p1", GHOST_PINK_1_FILEPATH);
		data_->assets.loadTexture("p2", GHOST_PINK_2_FILEPATH);
		data_->assets.loadTexture("p3", GHOST_PINK_3_FILEPATH);
		data_->assets.loadTexture("p4", GHOST_PINK_4_FILEPATH);

		data_->assets.loadTexture("i1", GHOST_BLUE_1_FILEPATH);
		data_->assets.loadTexture("i2", GHOST_BLUE_2_FILEPATH);
		data_->assets.loadTexture("i3", GHOST_BLUE_3_FILEPATH);
		data_->assets.loadTexture("i4", GHOST_BLUE_4_FILEPATH);

		data_->assets.loadTexture("c1", GHOST_ORANGE_1_FILEPATH);
		data_->assets.loadTexture("c2", GHOST_ORANGE_2_FILEPATH);
		data_->assets.loadTexture("c3", GHOST_ORANGE_3_FILEPATH);
		data_->assets.loadTexture("c4", GHOST_ORANGE_4_FILEPATH);
	}
	void PolyPacman::GameState::init()
	{
		loadGhostsTextures();
		
		std::string board = 
			"!#######################!"
			"!#          #          #!"
			"!# ## ##### # ##### ## #!"
			"!#                     #!"
			"!# ## # ######### # ## #!"
			"!#    #     #     #    #!"
			"!#### ##### # ##### ####!"
			"!!!!# #     C     # #!!!!"
			"##### # # ##### # # #####"
			"#       #  BPI  #       #"
			"##### # # ##### # # #####"
			"!!!!# #           # #!!!!"
			"!#### # ######### # ####!"
			"!#          #          #!"
			"!# ## ##### # ##### ## #!"
			"!#  #           @   #  #!"
			"!# ## # ######### # ## #!"
			"!# $  #     #     #    #!"
			"!# ####### ### ####### #!"
			"!# #     #     #     # #!"
			"!# # ### ## # ## ### # #!"
			"!# # #      #      # # #!"
			"!# # # #### # #### # # #!"
			"!#          #          #!"
			"!#######################!";
		board_ = std::make_shared<Board>();
		board_->init(BOARD_WIDTH, BOARD_HEIGHT, board);
		pacman_ = std::make_shared<Pacman>();

		pacman_->init(Direction::LEFT, board_);

		ghosts_[0] = std::make_shared<Ghost>(GhostId::BLINKY, data_, board_);
		ghosts_[0]->init(board_->getGhostSpawnPos(GhostId::BLINKY));

		ghosts_[1] = std::make_shared<Ghost>(GhostId::CLYDE, data_, board_);
		ghosts_[1]->init(board_->getGhostSpawnPos(GhostId::CLYDE));

		ghosts_[2] = std::make_shared<Ghost>(GhostId::INKY, data_, board_);
		ghosts_[2]->init(board_->getGhostSpawnPos(GhostId::INKY));

		ghosts_[3] = std::make_shared<Ghost>(GhostId::PINKY, data_, board_);
		ghosts_[3]->init(board_->getGhostSpawnPos(GhostId::PINKY));
	}

	void PolyPacman::GameState::handleInput()
	{
		sf::Event event;

		while (data_->window.pollEvent(event))
		{
			if (sf::Event::Closed == event.type)
			{
				data_->window.close();
			}
			if (event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
				case sf::Keyboard::W:
				{
					pacman_->setNextDirection(Direction::UP);
					break;
				}	
				case sf::Keyboard::S:
				{
					pacman_->setNextDirection(Direction::DOWN);
					break;
				}				
				case sf::Keyboard::A:
				{
					pacman_->setNextDirection(Direction::LEFT);
					break;
				}				
				case sf::Keyboard::D:
				{
					pacman_->setNextDirection(Direction::RIGHT);
					break;
				}
				default:
					break;
				}
			}
		}

	}

	void PolyPacman::GameState::update(float dt)
	{
		pacman_->update(dt);
		for (size_t i = 0; i < 4; i++)
		{
			ghosts_[i]->update(dt);
			if (pacman_->getGlobalBounds().intersects(ghosts_[i]->getGhostBounds()))
			{
				data_->machine.removeState();
			}
		}		
		if (board_->getCookieCount() == 0)
		{
			data_->machine.removeState();
		}
	}

	void PolyPacman::GameState::draw(float dt)
	{
		data_->window.clear();
		board_->draw(data_->window);
		pacman_->draw(data_->window);
		for (size_t i = 0; i < 4; i++)
		{
			ghosts_[i]->draw(data_->window);
		}
		data_->window.display();
	}
}
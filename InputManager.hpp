#pragma once

#include <SFML/Graphics.hpp>

namespace PolyPacman
{
	class InputManager
	{
	public:
		InputManager() {};
		~InputManager() = default;

		bool isSpriteClicked(sf::Sprite object, sf::Mouse::Button button, sf::RenderWindow &window) const;
		sf::Vector2i getMousePos( sf::RenderWindow &window) const;
	};
}

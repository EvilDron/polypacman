#pragma once

#include <unordered_map>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

namespace PolyPacman
{
	class AssetManager
	{
	public:
		AssetManager(){};
		~AssetManager() = default;

		void loadTexture(std::string name, std::string filename); // ���������� �������� � ���
		sf::Texture &getTexture(std::string name); // �������� ������ �� ������������ ��������

		void loadFont(std::string, std::string filename);// ���������� ����� � ���
		sf::Font &getFont(std::string name); // �������� ������ �� ������������ �����

	private:
		std::unordered_map<std::string, sf::Texture> textures_; // ��� ������� ������������ �������
		std::unordered_map<std::string, sf::Font> fonts_; // ��� ������� ������������ �������
	};
	
}

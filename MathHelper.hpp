#pragma once

#include <cmath>
#include <SFML/Graphics.hpp>

// �������� �������������� �������

float degreesToRadians(float degrees)
{
	return (degrees * 3.14159265358979323846 / 180);
}

sf::Vector2f polarToDecartian(float radius, float degrees)
{
	const float radians = degreesToRadians(degrees);

	return radius * sf::Vector2f(std::cos(radians), std::sin(radians));
}

sf::Vector2f getRadialPoint(float angle, float radius)
{
	return polarToDecartian(radius, angle - 90);
}

void applyShape(sf::ConvexShape& shape, std::vector<sf::Vector2f> const& points)
{
	const unsigned pointCount = unsigned(points.size());
	shape.setPointCount(pointCount);
	for (unsigned i = 0; i < pointCount; ++i)
	{
		shape.setPoint(i, points[i]);
	}
}


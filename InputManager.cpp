#include "InputManager.hpp"


namespace PolyPacman
{
	bool InputManager::isSpriteClicked(sf::Sprite object, sf::Mouse::Button button, sf::RenderWindow &window) const
	{
		if (sf::Mouse::isButtonPressed(button))
		{
			sf::Vector2f tempPos = object.getPosition();
			sf::FloatRect tempBounds = object.getGlobalBounds();
			sf::IntRect tempRect(tempPos.x, tempPos.y, tempBounds.width, tempBounds.height);
			
			return tempRect.contains(sf::Mouse::getPosition(window));
		}
		return false;
	}

	sf::Vector2i InputManager::getMousePos(sf::RenderWindow & window) const
	{
		return sf::Mouse::getPosition(window);
	}

}
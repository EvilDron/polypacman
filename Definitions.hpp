#pragma once

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 800


#define SPLASH_STATE_SHOW_TIME 3.0
#define SPLASH_STATE_BACKGROUND_FILEPATH "Resources/res/splash.jpg"

#define MAIN_MENU_BACKGROUND_FILEPATH "Resources/res/menu_background.png"

#define MAIN_MENU_BACKGROUND_FILEPATH "Resources/res/menu_background.png"

#define GHOST_BLUE_1_FILEPATH "Resources/res/BlueGhost1.png"
#define GHOST_BLUE_2_FILEPATH "Resources/res/BlueGhost2.png"
#define GHOST_BLUE_3_FILEPATH "Resources/res/BlueGhost3.png"
#define GHOST_BLUE_4_FILEPATH "Resources/res/BlueGhost4.png"

#define GHOST_ORANGE_1_FILEPATH "Resources/res/OrangeGhost1.png"
#define GHOST_ORANGE_2_FILEPATH "Resources/res/OrangeGhost2.png"
#define GHOST_ORANGE_3_FILEPATH "Resources/res/OrangeGhost3.png"
#define GHOST_ORANGE_4_FILEPATH "Resources/res/OrangeGhost4.png"

#define GHOST_PINK_1_FILEPATH "Resources/res/PinkGhost1.png"
#define GHOST_PINK_2_FILEPATH "Resources/res/PinkGhost2.png"
#define GHOST_PINK_3_FILEPATH "Resources/res/PinkGhost3.png"
#define GHOST_PINK_4_FILEPATH "Resources/res/PinkGhost4.png"

#define GHOST_RED_1_FILEPATH "Resources/res/RedGhost1.png"
#define GHOST_RED_2_FILEPATH "Resources/res/RedGhost2.png"
#define GHOST_RED_3_FILEPATH "Resources/res/RedGhost3.png"
#define GHOST_RED_4_FILEPATH "Resources/res/RedGhost4.png"

#define PACMAN_TITLE_FILEPATH "Resources/res/logo.png"
#define TEXT_FONT_FILEPATH "Resources/fonts/pacman.ttf"

#define BLOCK_SIZE 32
#define BOARD_WIDTH 25
#define BOARD_HEIGHT 25

static const float GHOST_SPEED = 120.f;
static const float GHOST_SIZE = 32.f; 

const sf::Color WALL_COLOR = sf::Color(52, 93, 199);
const sf::Color ROAD_COLOR = sf::Color(40, 40, 40);

const sf::Color COOKIE_COLOR = sf::Color(255, 255, 255);
const sf::Color PACMAN_COLOR = sf::Color(255, 216, 0);
const float PACMAN_SPEED = 120.0f;
const float PACMAN_RADIUS = 16.0f;
const float PACMAN_VISIBLE_RADIUS = 12.0f;
const float PACMAN_ANIMATION_PERIOD = 0.3f;
const float PACMAN_MOUTH_ANGLE = 110.f;
const float MAX_SHIFT = 0.5f * BLOCK_SIZE;
const float MIN_COOKIE_OVERLAP_AREA = 400.f;
const float COOKIE_RADIUS = 2.f;
const float SUPERCOOKIE_RADIUS = 5.f;
#include "SplashState.hpp"
#include <iostream>
#include "Definitions.hpp"
#include "MainMenuState.hpp"

namespace PolyPacman
{
	SplashState::SplashState(GameDataRef data):
		data_(data)
	{
	}

	void SplashState::init()
	{
		data_->assets.loadTexture("SplashStateTexture", SPLASH_STATE_BACKGROUND_FILEPATH);
		background_.setTexture(data_->assets.getTexture("SplashStateTexture"));
		background_.setPosition(SCREEN_WIDTH / 2 - background_.getGlobalBounds().width/2, SCREEN_HEIGHT / 2- background_.getGlobalBounds().height / 2);
	}

	void SplashState::handleInput()
	{
		sf::Event event;

		while (data_->window.pollEvent(event))
		{
			if (sf::Event::Closed == event.type)
			{
				data_->window.close();
			}
		}
	}

	void SplashState::update(float dt)
	{
		if (clock_.getElapsedTime().asSeconds() > SPLASH_STATE_SHOW_TIME)
		{
			data_->machine.addState(StateRef(new MainMenuState(data_)));
		}
	}

	void SplashState::draw(float dt)
	{
		data_->window.clear(sf::Color::White);
		data_->window.draw(background_);
		data_->window.display();
	}

}
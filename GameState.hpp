#pragma once
#include "State.hpp"
#include "Board.hpp"
#include "Game.hpp"
#include "Pacman.hpp"
#include "Ghost.hpp"

namespace PolyPacman
{
	typedef std::shared_ptr<Pacman> PacmanRef;

	class GameState : public State
	{
	public:
		GameState(GameDataRef data);
		~GameState() = default;

		virtual void init() override;
		virtual void handleInput() override;
		virtual void update(float dt) override;
		virtual void draw(float dt) override;

	private:
		void loadGhostsTextures();

		BoardRef board_;
		PacmanRef pacman_;
		GameDataRef data_;
		GhostRef ghosts_[4];

	};
}
#include "Button.hpp"

namespace PolyPacman
{
	Button::Button()
	{
		action_ = []() {};
	}
	Button::Button(const sf::Vector2f & position, const std::string & text, const sf::Font & font,
								 std::size_t characterSize, const std::function<void()> & action, const sf::Color & textColor)
	{
		text_.setPosition(position);
		text_.setString(text);
		text_.setCharacterSize(characterSize);
		text_.setFont(font);
		text_.setFillColor(textColor);
		action_ = std::move(action);
	}
	void Button::execute()
	{
		action_();
	}
	void Button::setPosition(const sf::Vector2f & position)
	{
		text_.setPosition(position);
	}
	void Button::setPosition(const float x, const float y)
	{
		text_.setPosition(x, y);
	}
	sf::Vector2f Button::getPosition() const
	{
		return text_.getPosition();
	}
	void Button::setText(const std::string &text)
	{
		text_.setString(text);
	}
	void Button::setFont(const sf::Font & font)
	{
		text_.setFont(font);
	}
	void Button::setCharacterSize(size_t size)
	{
		text_.setCharacterSize(size);
	}
	void Button::setAction(const std::function<void()> & action)
	{
		action_ = std::move(action);
	}
	void Button::setTextFillColor(const sf::Color & color)
	{
		text_.setFillColor(color);
	}
	void Button::setTextStyle(const sf::Uint32 & style)
	{
		text_.setStyle(style);
	}
	std::string Button::getText() const
	{
		return text_.getString();
	}
	sf::FloatRect Button::getGlobalBounds() const
	{
		return text_.getGlobalBounds();
	}
	void Button::draw(sf::RenderWindow & window)
	{
		window.draw(text_);
	}

}
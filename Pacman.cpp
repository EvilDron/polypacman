#include "Pacman.hpp"
#include <math.h>
#include "Definitions.hpp"
#include "MathHelper.hpp"
namespace PolyPacman
{

	void Pacman::init(const Direction& direction,  BoardRef board)
	{
		nextDirection_ = Direction::NONE;
		setDirection(direction);
		board_ = board;
		position_ = board_->getPlayerSpawnPos();
		eatenCookies_ = 0;
		animationPhase_ = 0.25;
	}
	void Pacman::update(const float dt)
	{
		const float step = PACMAN_SPEED * dt;

		tryRotate();

		sf::Vector2f movement(0.f, 0.f);
		switch (direction_)
		{
		case Direction::UP:
			movement.y -= step;
			break;
		case Direction::DOWN:
			movement.y += step;
			break;
		case Direction::LEFT:
			movement.x -= step;
			break;
		case Direction::RIGHT:
			movement.x += step;
			break;
		case Direction::NONE:
			break;
		}
		position_ += movement;

		if (board_->isWallIntersects(getGlobalBounds()))
		{
			position_ -= movement;
		}

		if (direction_ == Direction::NONE)
		{
			animationPhase_ = 0.25;
		}
		else
		{
			const float deltaPhase = dt / PACMAN_ANIMATION_PERIOD;
			animationPhase_ = fmodf(animationPhase_ + deltaPhase, 1.f);
		}
		if (board_->isCookieIntersects(getGlobalBounds()))
		{
			
		}
		updateShape();
	}
	void Pacman::tryRotate()
	{
		const float step = 32;
		if (nextDirection_ != Direction::NONE)
		{
			sf::Vector2f movement(0.f, 0.f);
			switch (nextDirection_)
			{
			case Direction::UP:
				movement.y -= step;
				break;
			case Direction::DOWN:
				movement.y += step;
				break;
			case Direction::LEFT:
				movement.x -= step;
				break;
			case Direction::RIGHT:
				movement.x += step;
				break;
			case Direction::NONE:
				break;
			}
			position_ += movement;

			if (!board_->isWallIntersects(getGlobalBounds()))
			{
				setDirection(nextDirection_);
				nextDirection_ = Direction::NONE;
			}
			position_ -= movement;
		}
	}
	void Pacman::draw(sf::RenderWindow& window)
	{
		sf::RenderStates states;
		states.transform.translate(position_);
		states.transform.rotate(orientationDegrees_);
		topShape_.setFillColor(PACMAN_COLOR);
		bottomShape_.setFillColor(PACMAN_COLOR);
		window.draw(topShape_, states);
		window.draw(bottomShape_, states);
	}
	void Pacman::setDirection(const Direction& direction)
	{
		direction_ = Direction::NONE;
		switch (direction)
		{
		case Direction::UP:
		{
			direction_ = Direction::UP;
			orientationDegrees_ = 0;
			break;
		}
		case Direction::DOWN:
		{
			direction_ = Direction::DOWN;
			orientationDegrees_ = 180;
			break;
		}
		case Direction::LEFT:
		{
			direction_ = Direction::LEFT;
			orientationDegrees_ = 270;
			break;
		}
		case Direction::RIGHT:
		{
			direction_ = Direction::RIGHT;
			orientationDegrees_ = 90;
			break;
		}
		}
	}
	void Pacman::setNextDirection(const Direction& direction)
	{
		nextDirection_ = direction;
	}
	sf::FloatRect Pacman::getGlobalBounds()
	{
		return sf::FloatRect(position_.x - PACMAN_RADIUS,
												 position_.y - PACMAN_RADIUS,
												 2.f * PACMAN_RADIUS,
												 2.f * PACMAN_RADIUS);
	}

	void Pacman::updateShape()
	{
		const float deviationPhase = 2.f * fabsf(0.5f - animationPhase_);
		const float deviationAngle = 0.5 * deviationPhase * PACMAN_MOUTH_ANGLE;

		const float radius = PACMAN_VISIBLE_RADIUS;
		std::vector<sf::Vector2f> points;
		sf::Vector2f center(0, 0.25f * radius);

		for (float angle = 180.f; angle >= deviationAngle; angle -= 5.f)
		{
			points.push_back(getRadialPoint(angle, radius));
		}
		points.push_back(center);
		applyShape(topShape_, points);
		points.clear();

		for (float angle = 180.f; angle <= 360.f - deviationAngle; angle += 5.f)
		{
			points.push_back(getRadialPoint(angle, radius));
		}
		points.push_back(center);
		applyShape(bottomShape_, points);
	}
	
}

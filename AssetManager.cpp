#include "AssetManager.hpp"

namespace PolyPacman
{
	void AssetManager::loadTexture(std::string name, std::string filename)
	{
		sf::Texture tex;
		if (tex.loadFromFile(filename))
		{
			textures_[name] = tex;
		}
	}

	sf::Texture & AssetManager::getTexture(std::string name)
	{
		return textures_.at(name);
	}

	void AssetManager::loadFont(std::string name, std::string filename)
	{
		sf::Font font;
		if (font.loadFromFile(filename))
		{
			fonts_[name] = font;
		}
	}

	sf::Font & AssetManager::getFont(std::string name)
	{
		return fonts_.at(name);
	}
}
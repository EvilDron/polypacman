#include "MainMenuState.hpp"
#include <iostream>
#include "Definitions.hpp"
#include "GameState.hpp"

namespace PolyPacman
{
	MainMenuState::MainMenuState(const GameDataRef &  data) :
		data_(data)
	{
	}

	void MainMenuState::init()
	{
		data_->assets.loadTexture("MainMenuBackground", MAIN_MENU_BACKGROUND_FILEPATH);
		data_->assets.loadTexture("GameTitle", PACMAN_TITLE_FILEPATH);
		data_->assets.loadFont("PacmanFont", TEXT_FONT_FILEPATH);
		background_.setTexture(data_->assets.getTexture("MainMenuBackground"));
		title_.setTexture(data_->assets.getTexture("GameTitle"));
		title_.setPosition(title_.getPosition().x, 50);

		GameDataRef tmpData = data_;
		ButtonRef tmpButton(std::make_shared<Button>());

		//StartButton
		tmpButton->setFont(data_->assets.getFont("PacmanFont"));
		tmpButton->setText("Start");
		tmpButton->setCharacterSize(60);
		tmpButton->setAction([tmpData]() { tmpData->machine.addState(StateRef(new GameState(tmpData)), false); });
		tmpButton->setTextFillColor(sf::Color::Yellow);
		tmpButton->setTextStyle(sf::Text::Bold);
		tmpButton->setPosition(SCREEN_WIDTH / 2 - tmpButton->getGlobalBounds().width / 2, SCREEN_HEIGHT / 2);
	
		buttons.push_back(tmpButton);
		//StartButton End			

		//ExitButton
		tmpButton = std::make_shared<Button>();
		tmpButton->setFont(data_->assets.getFont("PacmanFont"));
		tmpButton->setText("Exit");
		tmpButton->setCharacterSize(60);
		tmpButton->setAction([]() { std::exit(0); });
		tmpButton->setTextFillColor(sf::Color::Yellow);
		tmpButton->setTextStyle(sf::Text::Bold);
		tmpButton->setPosition(SCREEN_WIDTH / 2 - tmpButton->getGlobalBounds().width / 2, SCREEN_HEIGHT / 2 + 65);

		buttons.push_back(tmpButton);
		//ExitButton End		
	
	}

	void MainMenuState::handleInput()
	{
		sf::Event event;

		while (data_->window.pollEvent(event))
		{
			if (sf::Event::Closed == event.type)
			{
				data_->window.close();
			}
			if (sf::Event::KeyPressed == event.type)
			{
				buttons[0]->execute(); //Testing
			}
		}
	}

	void MainMenuState::update(float dt)
	{
		
	}

	void MainMenuState::draw(float dt)
	{
		data_->window.clear();
		data_->window.draw(background_);
		data_->window.draw(title_);
		for (ButtonRef button : buttons)
		{
			button->draw(data_->window);
		}

		data_->window.display();
	}

}
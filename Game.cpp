#include "Game.hpp"
#include "SplashState.hpp"


namespace PolyPacman
{
	Game::Game(int width, int height, std::string title)
	{
		data_->window.create(sf::VideoMode(width, height), title, sf::Style::Close | sf::Style::Titlebar);
		data_->machine.addState(StateRef(new SplashState(data_)));
		run(); 
	}
	void Game::run()
	{
		float newTime;
		float frameTime;
		float interpolation;
			
		float currentTime = clock_.getElapsedTime().asSeconds();
		float accumulator = 0.0f;

		while (data_->window.isOpen())
		{
			data_->machine.proccessStateChanges();
			newTime = clock_.getElapsedTime().asSeconds();
			frameTime = newTime - currentTime;
			if (frameTime >= 0.25f)
			{
				frameTime = 0.25f;
			}
			currentTime = newTime;
			accumulator += frameTime;
			while (accumulator >= dt)
			{
				data_->machine.getActiveState()->handleInput();
				data_->machine.getActiveState()->update(dt);
				accumulator -= dt;
			}

			interpolation = accumulator / dt;
			data_->machine.getActiveState()->draw(interpolation);
		}
	}
}
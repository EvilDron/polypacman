#pragma once
#include <SFML/Graphics.hpp>
#include "State.hpp"
#include "Game.hpp"

namespace PolyPacman
{
	class SplashState : public State
	{
	public:
		SplashState(GameDataRef data);
		~SplashState() = default;
		
		void init();
		void handleInput();
		void update(float dt);
		void draw(float dt);

	private:
		GameDataRef data_;
		sf::Clock clock_;
		sf::Sprite background_;
	};
}

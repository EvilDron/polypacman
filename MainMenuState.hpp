#pragma once
#include <vector>
#include <SFML/Graphics.hpp>
#include "State.hpp"
#include "Button.hpp"
#include "Game.hpp"

namespace PolyPacman
{
	class MainMenuState : public State
	{
	public:
		MainMenuState(const GameDataRef & data);
		~MainMenuState() = default;

		void init();
		void handleInput();
		void update(float dt);
		void draw(float dt);

	private:
		GameDataRef data_;
		sf::Sprite background_;
		sf::Sprite title_;
		
		std::vector<ButtonRef> buttons;
	};
}
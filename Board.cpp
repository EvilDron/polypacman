#include "Board.hpp"
#include "Definitions.hpp"

namespace PolyPacman
{

	Board::Board() :
		width_(0),
		height_(0),
		cookie_count(0)
	{
		cells_.clear();
	}
	void Board::initBoardGraphics()
	{
		graphics_.cookieShape.setRadius(COOKIE_RADIUS);
		graphics_.cookieShape.setFillColor(COOKIE_COLOR);
		graphics_.superCookieShape.setRadius(SUPERCOOKIE_RADIUS);
		graphics_.superCookieShape.setFillColor(COOKIE_COLOR);
	}
	sf::Vector2f Board::getStartPosition(const char sign) const
	{
		for (size_t y = 0; y < width_; y++)
		{
			for (size_t x = 0; x < height_; x++)
			{
				const size_t offset = x + y * width_;
				if (field_[offset] == sign)
				{
					return sf::Vector2f (x * BLOCK_SIZE,y * BLOCK_SIZE);
				}
			}
		}
		return { 0, 0 };
	}
	void Board::init(const std::size_t width, const std::size_t height, const std::string field)
	{
		initBoardGraphics();

		width_ = width;
		height_ = height;
		field_ = field;
		for (size_t y = 0; y < width_; y++)
		{
			for (size_t x = 0; x < height_; x++)
			{
				const size_t offset = x + y * width_;
				CellType category;
				sf::Color color;
				bool containCookie = false;
				if (field[offset] == '#')
				{
					category = CellType::WALL;
					color = WALL_COLOR;
				}
				else
				{
					category = CellType::EMPTY;
					if (field[offset] != '!')
					{
						containCookie = true;
						cookie_count++;
					}
					color = ROAD_COLOR;
				}
				CellRef cell = std::make_shared<Cell>();
				cell->containCookie = containCookie;
				cell->type = category;
				cell->shape.setPosition(x * BLOCK_SIZE, y * BLOCK_SIZE);
				cell->shape.setSize(sf::Vector2f(BLOCK_SIZE, BLOCK_SIZE));
				cell->shape.setFillColor(color);
				cells_.push_back(std::move(cell));
				if (field[offset] == '@')
				{
					playerSpawnPoint_ = sf::Vector2f( x * BLOCK_SIZE + BLOCK_SIZE / 2, y * BLOCK_SIZE + BLOCK_SIZE / 2);
				}
			}
		}

	}
	sf::Vector2f Board::getPlayerSpawnPos() const
	{
		return playerSpawnPoint_;
	}


	sf::Vector2f Board::getGhostSpawnPos(GhostId ghostId) const
	{
		switch (ghostId)
		{
		case GhostId::BLINKY:
			return getStartPosition('B');
		case GhostId::PINKY:
			return getStartPosition('P');
		case GhostId::INKY:
			return getStartPosition('I');
		case GhostId::CLYDE:
			return getStartPosition('C');
		default:
			break;
		}
	}
	bool Board::isWallIntersects(const sf::FloatRect & rect) const
	{
		for (auto cell : cells_)
		{
			if (cell->type == CellType::WALL && cell->shape.getGlobalBounds().intersects(rect))
			{
				return true;
			}
		}
		return false;
	}
	bool Board::isCookieIntersects(const sf::FloatRect & rect)
	{
		for (auto cell : cells_)
		{
			graphics_.cookieShape.setPosition(cell->shape.getPosition().x + BLOCK_SIZE / 2 - COOKIE_RADIUS, cell->shape.getPosition().y + BLOCK_SIZE / 2 - COOKIE_RADIUS);

			if (cell->type == CellType::EMPTY && cell->containCookie && graphics_.cookieShape.getGlobalBounds().intersects(rect))
			{
				cell->containCookie = false;
				cookie_count--;
				return true;
			}
		}
		return false;
	}

	

	void Board::draw(sf::RenderWindow& window)
	{
		for (CellRef cell : cells_)
		{
			window.draw(cell->shape);
			if (cell->containCookie)
			{
				graphics_.cookieShape.setPosition(cell->shape.getPosition().x+BLOCK_SIZE/2 - COOKIE_RADIUS, cell->shape.getPosition().y + BLOCK_SIZE / 2 - COOKIE_RADIUS);
				window.draw(graphics_.cookieShape);
			}
		}
	}
	size_t Board::getCookieCount() const
	{
		return cookie_count;
	}
}